const readline = require('readline');
const fs = require('fs');

var myArgs = process.argv.slice(2);
// DEBUG:
// console.log('myArgs: ', myArgs[0]);


// create instance of readline
// each instance is associated with single input stream
let rl = readline.createInterface({
    input: fs.createReadStream(myArgs[0])
});


// array containing the lines from the input text files
var input = [];
// array containing the number of rows and columns in the grid
var gridsize = [];
// array containing the contents of the grid
var grid = [];
// array containing the list of words to be found in the grid
var words = [];

// entry point to start reading the input file to identify the grid/words to find
readinputfile();

// METHOD: to parse the input file containing information about the grid and 
function readinputfile() {
let line_no = 0;

// event is emitted after each line
rl.on('line', function(line) {
    line_no++;

    // DEBUG:
    // console.log(line);

    // store the input stream lines in an array to build the grid and words
    input.push(line);

    if(line_no == 1) {
        // read grid size
        gridsize = line.split('x');
	gridsize[0] = parseInt(gridsize[0]);
	gridsize[1] = parseInt(gridsize[1]);
        // DEBUG:
        // console.log("Num rows: " + gridsize[0]);
        // console.log("Num cols: " + gridsize[1]);
    }    
});

// end
rl.on('close', function(line) {
    // DEBUG:
    // console.log(input);
    // console.log('Total lines : ' + line_no);
    
    // build the grid by reading the rows with grid content:
    buildgrid(gridsize[0]);
    // identify and parse the list of words from the input file:
    lookforwords(gridsize[0]);
});
}

function findwords() {
    words.forEach(word => { 
        // console.log(word); 
	var check = lookforword(word);
	if(check != "-") {
	    // console.log(check);
	}
    });
}

function lookforword(word) {
    for(i=0; i<gridsize[0]; i++) {
	for(j=0; j<gridsize[1]; j++) {
	    if(word[0] == grid[i][j]) {
		var check = iswordingrid(i,j,0,word,"-");		
	    }
	}
    }
    return "-";
}

function iswordingrid(row,column,index,word,direction) {
    if(index >= word.length) {
	return "-";
    }

    // DEBUG:
    // console.log("row: " + row + ", column: " + column + ", index: " + index + " word: " + word);

    if(word[index] == grid[row][column]) {

	// if this is the last index in the word, return the coordinates
	if(index == (word.length-1)) {
	    // DEBUG:
    	    // console.log("row: " + row + ", column: " + column + ", index: " + index + " word: " + word);
	    console.log(word + " " + i + ":" + j + " " + row + ":" + column);
	    return (row + ":" + column);
	}

	// look for the next letter in all directions:
	// look vertically
	if((row+1) < gridsize[0] && (direction == "-" || direction == "down")) {
	    iswordingrid(row+1,column,index+1,word,"down");
	}
	if(row > 0 && (direction == "-" || direction == "up")) {
	    iswordingrid(row-1,column,index+1,word,"up");
	}
	
	// look horizontally
	if((column+1) < gridsize[1] && (direction == "-" || direction == "right")) {
	    iswordingrid(row,column+1,index+1,word,"right");
	}
	if(column > 0 && (direction == "-" || direction == "left")) {
	    iswordingrid(row,column-1,index+1,word,"left");
	}
	
	// look diagonally
	if((row+1) < gridsize[0] && (column+1) < gridsize[1] && (direction == "-" || direction == "d-right-down")) {
	    iswordingrid(row+1,column+1,index+1,word,"d-right-down");
	}
	if((row+1) < gridsize[0] && column > 0 && (direction == "-" || direction == "d-left-down")) {
	    iswordingrid(row+1,column-1,index+1,word,"d-left-down");
	}
	if(row > 0 && (column+1) < gridsize[1] && (direction == "-" || direction == "d-right-up")) {
	    iswordingrid(row-1,column+1,index+1,word,"d-right-up");
	}
	if(row > 0 && column > 0 && (direction == "-" || direction == "d-left-up")) {
	    iswordingrid(row-1,column-1,index+1,word,"d-left-up");
	}

    }

    // console.log(grid[row][column]);
    return "-";
}

function buildgrid(numrows) {
    // DEBUG:
    // console.log(numrows);

    var row = [];
    var buildrow = 0;

    while(buildrow < numrows) {
	// DEBUG:
        // console.log("Current row: " + input[buildrow+1]);
	row = input[buildrow+1].split(' ');
	grid.push(row);
	buildrow++;
    }

    // DEBUG:
    // console.log(grid);
    
}

function lookforwords(numrows) {
    // DEBUG:
    // console.log(numrows);
    // console.log("input length: " + (input.length - numrows - 1));

    // starting index for the words list in the input stream
    var buildword = parseInt(numrows,10) + 1;

    while(buildword < input.length) {
	// DEBUG:
        // console.log("Current row: " + input[buildword]);

	// remove spaces from the word prior to storing in the array
	var word = input[buildword].split(' ').join('');
	words.push(word);

	buildword++;
    }

    // DEBUG:
    // console.log(words); 
    findwords();
}